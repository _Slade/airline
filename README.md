# Airline

Performs various graph-based operations (Djikstra's algorithm, BFS, DFS,
Kruskal's algorithm, etc.) on user-defined data. This is an interactive program.

## Operation

On startup, you will be prompted for a file in the format:

    int ::= [1-9][0-9]+
    dollar_value ::= int '.' [0-9][0-9]
    location_count ::= int 
    location ::= [A-Za-z]+
    edge ::= int int int dollar_value

    file ::= location_count '\n'
             location       '\n' # location_count times
             edge           '\n' # zero to 2^location_count times, inclusive

Example:
    
    2
    A
    B
    1 2 50 100.00

Specifies a graph with two nodes, `A` and `B`, with one edge between them of
length 50 and price 100.00.

After entering the graph data, you will be presented with the following menu:

    Select an action.
    (1) Show the current list of routes, distances, and prices
    (2) Display MST by distance
    (3) Show shortest path by miles
    (4) Show shortest path by price
    (5) Show path with fewest hops
    (6) Show all trips cheaper than a given amount
    (7) Add a new route between existing locations
    (8) Remove a route
    (9) Quit

From here, you can simply enter the number of an action and it will guide you
through the process.
