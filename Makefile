JC      = javac
JR      = java
JCFLAGS = -Xlint
D       = project4
MAIN    = ./$(D)/Airline
CLASSES = $(addprefix ./$(D)/, \
		Bag                    \
		Global                 \
		BreadthFirstPaths      \
		CC                     \
		DijkstraUndirectedSP   \
		Edge                   \
		Graph                  \
		Function               \
		IO                     \
		IndexMinPQ             \
		Pair                   \
		Paths                  \
		Prices                 \
		PrimMST                \
		Queue                  \
		Stack                  \
		TripFinder             \
		UF                     \
		Util                   \
	)
DEPS    = $(addsuffix .java, $(CLASSES))

RM = rm -fv --

default : $(MAIN).class

$(MAIN).class : $(MAIN).java $(DEPS)
	$(JC) $(JCFLAGS) $<

run : $(MAIN).class
	$(JR) -ea $(D).$(notdir $(MAIN))

tags : $(MAIN).java $(DEPS)
	ctags -R ./$(D)

clean :
	$(RM) ./$(D)/*.class ./$(D)/classes/*.class
