/** Functions for manipulating prices */
package project4;
public class Prices
{
    private Prices() { }

    /**
     * Parses a price in the form [0-9]+\.[0-9]{2} into an int representing
     * the number of cents (dollars * 100 + cents).
	 * @param price
     **/
    public static int parsePrice(String price)
    {
        if (!price.matches("\\d+\\.\\d{2}"))
            throw new IllegalArgumentException(
                "Bad price format: " + price
                + ", should be dollars.cents"
            );

        int p = 0, e = 1;
        for (int i = 0; i < price.length() - 2; ++i)
            e *= 10;

        for (int i = 0; i < price.length(); ++i)
        {
            char c = price.charAt(i);
            if (c == '.') continue;
            p += (c - '0') * e;
            e /= 10;
        }

        return p;
    }

    /**
     * Converts a price (as returned by {@code parsePrice()}) into a string.
     * @param p
     */
    public static String toString(int p)
    {
        StringBuilder s = new StringBuilder();
        s.append(p / 100).append('.');
        int cents = p % 100;
        if (cents < 10)
            s.append('0');
        return s.append(p % 100).toString();
    }
}
