/** A collection of miscellaneous utility functions. */
package project4;
public class Util
{
    private Util() { /* Not instantiable */ }

    public static final String NL = System.lineSeparator();

    /**
     * Add two integers, throwing an exception on overflow.
     * @param x
     * @param y
     * @return The sum of {@code x} and {@code y}.
     * @throws ArithmeticException If overflow or underflow occurred.
     */
    public static int add(int x, int y)
    {
        int r = x + y;
        // Algorithm from Hacker's Delight
        if (((x ^ r) & (y ^ r)) < 0) throw new ArithmeticException("overflow");
        return r;
    }

    /**
     * Multiply two integers, throwing an exception on overflow.
     * @param x
     * @return The product of {@code x} and {@code y}.
     * @throws ArithmeticException If overflow or underflow occurred.
     */
    public static int mul(int x, int y)
    {
        long r = (long)x * (long)y;
        if ((int)r != r) throw new ArithmeticException("overflow");
        return (int)r;
    }

    public static void debugException(Exception e)
    {
        if (Global.DEBUG)
            e.printStackTrace();
        else
            System.out.println(e.getMessage());
    }

    /**
     * Throw an exception indicating that something is unimplemented.
     */
    public static void unimplemented()
    {
        throw new RuntimeException("Unimplemented");
    }
}
