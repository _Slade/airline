package project4;
import java.util.Map;
import java.util.HashMap;
public class Graph
{
    private final int V;
    private int E;
    private Bag<Edge>[] adj;
    private String[]             names = null;
    private Map<String, Integer> nums  = new HashMap<String, Integer>();

    /** A value guaranteed to not be a valid vertex. */
    public static final int NOT_VERTEX = -1;

    /**
     * Initializes an empty edge-weighted graph with <tt>V</tt> vertices and 0 edges.
     *
     * @param  V the number of vertices
     * @param names The names of each vertice.
     * @throws IllegalArgumentException if <tt>V</tt> < 0
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Graph(int V, String[] names)
    {
        if (V < 0) throw new IllegalArgumentException(
            "Number of vertices must be nonnegative"
        );

        this.V = V;
        this.E = 0;
        adj = (Bag<Edge>[]) new Bag[V];

        for (int v = 0; v < V; v++)
            adj[v] = new Bag<Edge>();

        if (names.length != V) throw new IllegalArgumentException(
            "Must have same number of names as vertices"
        );

        this.names = names.clone();
        for (int i = 0; i < names.length; ++i)
            nums.put(names[i], new Integer(i));
    }

    /**
     * Initializes a new edge-weighted graph that is a deep copy of <tt>G</tt>.
     *
     * @param  G the edge-weighted graph to copy
     */
    public Graph(Graph G)
    {
        this(G.V(), G.names);
        this.E = G.E();

        for (int v = 0; v < G.V(); v++)
        {
            // reverse so that adjacency list is in same order as original
            Stack<Edge> reverse = new Stack<Edge>();
            for (Edge e : G.adj[v])
                reverse.push(e);
            for (Edge e : reverse)
                adj[v].add(e);
        }

        this.names = G.names.clone();
        for (int i = 0; i < names.length; ++i)
            nums.put(names[i], new Integer(i));
    }

    /**
     * Returns the number of vertices in this edge-weighted graph.
     *
     * @return the number of vertices in this edge-weighted graph
     */
    public int V()
    {
        return V;
    }

    /**
     * Returns the number of edges in this edge-weighted graph.
     *
     * @return the number of edges in this edge-weighted graph
     */
    public int E()
    {
        return E;
    }

    // throw an IndexOutOfBoundsException unless 0 <= v < V
    private void validateVertex(int v)
    {
        if (v < 0 || v >= V) throw new IndexOutOfBoundsException(
            "vertex " + v + " is not between 0 and " + (V - 1)
        );
    }

    /**
     * Adds the undirected edge <tt>e</tt> to this edge-weighted graph.
     *
     * @param  e the edge
     * @throws IndexOutOfBoundsException unless both endpoints are between 0 and V-1
     */
    public void addEdge(Edge e)
    {
        int v = e.either();
        int w = e.other(v);
        validateVertex(v);
        validateVertex(w);
        adj[v].add(e);
        adj[w].add(e);
        E++;
    }

    /**
     * Returns the edges incident on vertex <tt>v</tt>.
     *
     * @param  v the vertex
     * @return the edges incident on vertex <tt>v</tt> as an Iterable
     * @throws IndexOutOfBoundsException unless {@code 0 <= v < V}
     */
    public Iterable<Edge> adj(int v)
    {
        validateVertex(v);
        return adj[v];
    }

    /**
     * Returns the degree of vertex <tt>v</tt>.
     *
     * @param  v the vertex
     * @return the degree of vertex <tt>v</tt>
     * @throws IndexOutOfBoundsException unless 0 <= v < V
     */
    public int degree(int v)
    {
        validateVertex(v);
        return adj[v].size();
    }

    /**
     * Returns all edges in this edge-weighted graph.
     * To iterate over the edges in this edge-weighted graph, use foreach notation:
     * <tt>for (Edge e : G.edges())</tt>.
     *
     * @return all edges in this edge-weighted graph, as an iterable
     */
    public Iterable<Edge> edges()
    {
        Bag<Edge> list = new Bag<Edge>();

        for (int v = 0; v < V; v++)
        {
            int selfLoops = 0;

            for (Edge e : adj(v))
            {
                if (e.other(v) > v)
                    list.add(e);
                // only add one copy of each self loop (self loops will be consecutive)
                else if (e.other(v) == v)
                {
                    if (selfLoops % 2 == 0) list.add(e);

                    selfLoops++;
                }
            }
        }

        return list;
    }

    /**
     * Get the name of a vertex.
     * @param v The number of the vertex.
     * @return The name of the vertex.
     * @throws NoSuchElementException If this vertex is not in the graph.
     */
    public String nameOf(int v)
    {
        validateVertex(v);
        return names[v];
    }

    /**
     * Checks if this graph has a vertex associated with the number {@code v}.
     **/
    public boolean hasVertex(int v)
    {
        return 0 <= v && v < V();
    }

    /**
     * Checks if this graph has a vertex associated with the name {@code v}.
     **/
    public boolean hasVertex(String v)
    {
        return nums.containsKey(v);
    }

    /**
     * Get the vertex associated with the name {@code v}.
     * @param v The name of the vertex.
     * @return The associated vertex number, or -1 if there is none.
     */
    public int idOf(String v)
    {
        Integer r = nums.get(v);
        if (r == null) throw new java.util.NoSuchElementException();
        return r;
    }

    /**
     * Get an {@code Iterable<String>} of this graph's locations' names.
     */
    public Iterable<String> names()
    {
        return java.util.Arrays.asList(names);
    }

    /**
     * Removes edges with the same endpoints as {@code e}.
     */
    public boolean remove(Edge e)
    {
        return adj[e.either()].remove(e)
            && adj[e.other(e.either())].remove(e);
    }

    /**
     * A toString() suitable for displaying to users.
     * @return A string representation of this Graph.
     */
    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        for (int v = 0; v < V(); ++v)
        {
            s.append("From " + names[v] + ": ");
            for (Edge e : adj(v))
                s.append(
                    String.format(
                        "%s (%d, %s) ",
                        names[e.other(v)],
                        e.getDistance(),
                        Prices.toString(e.getPrice())
                    )
                );
            s.append(System.lineSeparator());
        }

        return s.toString();
    }
} // END OF CLASS

/******************************************************************************
 *  Copyright 2002-2015, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/
