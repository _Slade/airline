/******************************************************************************
 *  Weighted edge.
 ******************************************************************************/

package project4;

/**
 * The <tt>Edge</tt> class represents a weighted edge in an {@link Graph}. Each
 * edge consists of two integers (naming the two vertices) and two weights. The
 * data type provides methods for accessing the two endpoints of the edge and
 * the weights.
 *
 * @author Robert Sedgewick
 * @author Kevin Wayne
 * @author Christopher C
 */
public class Edge implements Comparable<Edge>
{
    private final int v;
    private final int w;
    private int distance;
    private int price;

    /**
     * Initializes an edge between vertices {@code v} and {@code w} of the
     * given weights.
     *
     * @param  v one vertex
     * @param  w the other vertex
     * @param  distance the first weight of this edge
     * @param  price the second weight of this edge
     * @throws IndexOutOfBoundsException if either <tt>v</tt> or <tt>w</tt>
     *         is a negative integer
     */
    public Edge(int v, int w, int distance, int price)
    {
        if (v < 0) throw new IndexOutOfBoundsException(
            "Vertex name must be a nonnegative integer"
        );

        if (w < 0) throw new IndexOutOfBoundsException(
            "Vertex name must be a nonnegative integer"
        );

        if (v == w) throw new IllegalArgumentException(
            "Self-loops are not permitted"
        );

        this.v       = Math.min(v, w);
        this.w       = Math.max(v, w);
        this.distance = distance;
        this.price = price;
    }

    /**
     * Returns the first weight of this edge.
     *
     * @return the first weight of this edge
     */
    public int getDistance()
    {
        return distance;
    }

    public void setWeight(int w)
    {
        if (w < 0) throw new IllegalArgumentException("Weight cannot be negative");
        this.distance = w;
    }

    /**
     * Returns the second weight of this edge.
     *
     * @return the second weight of this edge
     */
    public int getPrice()
    {
        return price;
    }

    public void setPrice(int w)
    {
        if (w < 0) throw new IllegalArgumentException("Weight cannot be negative");
        this.price = w;
    }

    /**
     * Returns either endpoint of this edge.
     *
     * @return either endpoint of this edge
     */
    public int either()
    {
        return v;
    }

    /**
     * Returns the endpoint of this edge that is different from the given vertex.
     *
     * @param  vertex one endpoint of this edge
     * @return the other endpoint of this edge
     * @throws IllegalArgumentException if the vertex is not one of the
     *         endpoints of this edge
     */
    public int other(int vertex)
    {
        if (vertex == v) return w;
        else if (vertex == w) return v;
        else throw new IllegalArgumentException("Illegal endpoint");
    }

    /* These are because the .either()/.other() don't make sense semantically
       as guaranteeing the return of a specific endpoint. */

    /** Returns the vertex with the smaller number. */
    public int smaller()
    {
        return this.v;
    }

    /** Returns the vertex with the larger number. */
    public int larger()
    {
        return this.w;
    }

    /**
     * Compares two edges by distance.
     * Note that <tt>compareTo()</tt> is not consistent with <tt>equals()</tt>,
     * which uses the reference equality implementation inherited from
     * <tt>Object</tt>.
     *
     * @param  that the other edge
     * @return a negative integer, zero, or positive integer depending on
     *         whether the distance of this is less than, equal to, or greater
     *         than the argument edge
     */
    @Override
    public int compareTo(Edge that)
    {
        return this.compareTo1(that);
    }

    public int compareTo1(Edge that)
    {
        return this.getDistance() < that.getDistance()
            ? -1
            : this.getDistance() > that.getDistance()
            ? +1
            :  0;
    }

    public int compareTo2(Edge that)
    {
        return this.getPrice() < that.getPrice()
            ? -1
            : this.getPrice() > that.getPrice()
            ? +1
            :  0;
    }

    public static Function<Edge, Integer> getDist = (new Function<Edge, Integer>() {
        public Integer apply(Edge e) { return e.getDistance(); }
    });

    public static Function<Edge, Integer> getPrice = (new Function<Edge, Integer>() {
        public Integer apply(Edge e) { return e.getPrice(); }
    });

    /** Checks if two Edges have the same endpoints. */
    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof Edge))
            return false;
        if (o == this)
            return true;
        Edge that = (Edge)o;
        return this.v == that.v && this.w == that.w
            || this.v == that.w && this.w == that.v;
    }

    /**
     * Returns a string representation of this edge.
     *
     * @return a string representation of this edge
     */
    public String toString()
    {
        return String.format("%d-%d %d %d", v, w, distance, price);
    }
}

/******************************************************************************
 *  Copyright 2002-2015, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/
