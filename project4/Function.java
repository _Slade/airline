package project4;
public interface Function<T, R>
{
    R apply(T t);
}
