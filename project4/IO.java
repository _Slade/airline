package project4;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
public class IO
{
    private IO() { }

    public static final Console console = System.console();
    public static final String BAD_NUM_MSG = "Sorry, couldn't understand your"
                                           + " input. Please try again.";
    public static final String NL = Util.NL;

    /**
    * Reads a line from stdin, checking for ^D (input closed).
    * @return The line read from stdin.
    */
    public static String getLineChecked()
    {
        String line = console.readLine();
        if (line == null)
        {
            System.out.println("Reached end of input stream, exiting.");
            System.exit(0);
        }
        return line;
    }

    /**
     * Displays a prompt asking for a value, and returns the user's answer.
     * <code>prompt()</code> will append a single colon (':') and a space to
     * prompt messages, unless the message is the empty string.
     * @param message The message to display in the prompt.
     * @return The user's response.
     */
    public static String prompt(String message)
    {
        if (message.length() != 0)
            System.out.print(message + ": ");
        return getLineChecked().trim();
    }

    /**
     * Prompts the user for an integer value.
     * @param message The message to print in the prompt. .
     * @return When successful, returns the integer entered by the user. If
     * the entered message is the empty string, <code>null</code> is returned.
     * If the number fails to parse, the value of the String BAD_NUM_MSG is
     * printed, and the method will continue trying until a valid or empty input
     * is given.
     */
    public static Integer promptNum(String message)
    {
        while (true)
        {
            String rsp = prompt(message);
            if (rsp.length() == 0)
                return null;
            Integer i;
            try { i = Integer.parseInt(rsp); }
            catch (NumberFormatException e)
            {
                System.out.println(BAD_NUM_MSG);
                continue;
            }
            return i;
        }
    }
}
