package project4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Objects;

public class Airline
{
    // I recommend searching for these invidual enum constants to get to the
    // appropriate case where the code implementing them is.
    static enum MenuOpt
    {
        SHOW("(1) Show the current list of routes, distances, and prices"),
        DMST("(2) Display MST by distance"                               ),
        SPBM("(3) Show shortest path by miles"                           ),
        SPBP("(4) Show shortest path by price"                           ),
        SPBH("(5) Show path with fewest hops"                            ),
        CHPR("(6) Show all trips cheaper than a given amount"            ),
        ADDR("(7) Add a new route between existing locations"            ),
        REMR("(8) Remove a route"                                        ),
        QUIT("(9) Quit"                                                  );
        private final String menu;
        MenuOpt(String s) { menu = Objects.requireNonNull(s); }
        public String toString() { return this.menu; }
    }

    /**
     * Prompts for and reads a number from the user, returning its corresponding
     * menu option.
     * @return The option selected by the user, or null if the number is not the
     * number of a valid option.
     */
    public static MenuOpt getOpt()
    {
        Integer r = IO.promptNum("");
        if (r == null)
        {
            System.out.println("Please enter the number of an action.");
            return null;
        }
        --r;
        if (r < 0 || r >= MenuOpt.values().length)
        {
            System.out.println(
                "Sorry, the number given was not that of a valid action."
                + " Please try again."
            );
            return null;
        }
        return (MenuOpt.values())[r];
    }

    /**
     * Loads the routes given by {@code fileName} into {@code g} and returns a
     * pair consisting of an array of the location names and the graph created.
     * Note that the city numbers are indexed at 1, so you must subtract 1 when
     * retrieving a specific city from the array.
     * <p>
     * This probably ought to be a constructor in Graph.java.
     * @param f The file to load.
     *
     * @throws FileNotFoundException If {@code f} does not exist.
     * @return object
     **/
    public static Graph loadFile(File f)
        throws FileNotFoundException, IOException
    {
        if (!f.exists()) throw new FileNotFoundException(
            "File '" + f + "' does not exist"
        );

        // Since Scanner is a piece of garbage that can't do error reporting
        LineNumberReader in = new LineNumberReader(new FileReader(f));
        Graph g = null;
        try
        {
            int V = Integer.parseInt(in.readLine());
            String[] vertices = new String[V];
            for (int i = 0; i < V; ++i) // Read location names
            {
                String v = in.readLine();
                vertices[i] = v;
            }

            g = new Graph(V, vertices);

            while (in.ready()) // Read route data
            {
                int v1, v2, d, p;
                String[] fields = in.readLine().split("\\s+", 4);

                v1 = Integer.parseInt(fields[0]) - 1;
                v2 = Integer.parseInt(fields[1]) - 1;
                d  = Integer.parseInt(fields[2]);
                p  = Prices.parsePrice(fields[3]);
                g.addEdge(new Edge(v1, v2, d, p));
            }
        }
        catch (NumberFormatException e)
        {
            System.err.println("Bad number at line " + in.getLineNumber());
            e.printStackTrace();
            return null;
        }
        catch (IllegalArgumentException e)
        {
            System.err.println("Bad price at line " + in.getLineNumber());
            e.printStackTrace();
            return null;
        }
        catch (Exception e)
        {
            System.err.println(
                "Error occurred when parsing at line " + in.getLineNumber()
            );
            e.printStackTrace();
            return null;
        }
        finally
        {
            in.close();
        }

        return g;
    }

    /**
     * Dumps {@code g} to the file {@code f}.
	 * @param f The file to dump to.
	 * @param g The Graph to dump out.
     **/
    public static void dumpFile(File f, Graph g)
    {
        try (PrintStream out = new PrintStream(f))
        {
            dumpFile(out, g);
        }
        catch (FileNotFoundException e)
        {
            System.err.println("This should never happen");
            e.printStackTrace();
        }
    }

    public static void dumpFile(PrintStream out, Graph g)
    {
        out.println(g.V());        // Number of vertices
        for (String s : g.names()) // Names of vertices
            out.println(s);
        for (Edge e : g.edges())
            out.println(
                  (e.either() + 1)          + " "
                + (e.other(e.either()) + 1) + " "
                + e.getDistance()           + " "
                + Prices.toString(e.getPrice())
            );
    }

    /**
     * Prompt the user for a location by name and return its associated number.
     * @param prompt The message to prompt the user with.
     * @param graph  The Graph containing the locations.
     * @return The associated number, or Graph.NOT_VERTEX if none.
     */
    public static int promptForLocation(String prompt, Graph graph)
    {
        int v = Graph.NOT_VERTEX;
        while (true)
        {
            String s = IO.prompt(prompt);
            if (s.length() == 0) return Graph.NOT_VERTEX;
            if (graph.hasVertex(s))
                v = graph.idOf(s);
            else
            {
                System.out.println("That isn't a valid location.");
                continue;
            }
            break;
        }
        return v;
    }

    /**
     * Prompts for a price, returning a non-null Integer for a successful price
     * and null if the user backed out.
     * @param prompt The prompt message to display.
     */
    public static Integer promptForPrice(String prompt)
    {
        Integer price;
        RETRY:
        while (true)
        {
            String r = IO.prompt(prompt);
            if (r.length() == 0) return null;
            try { price = Prices.parsePrice(r); }
            catch (IllegalArgumentException e)
            {
                Util.debugException(e);
                continue RETRY;
            }
            break RETRY;
        }
        return price;
    }

    /**
     * Runs through quit sequence.
     * @return False if it's not okay to exit, true otherwise.
     */
    public static boolean quitMenu(File data, Graph graph) throws IOException
    {
        String r = IO.prompt("Would you like to save your changes (if any)? [yn]");
        if (r.length() == 0) return false;
        if (r.equals("y"))
        {
            r = IO.prompt("Overwrite existing file? [yn]");
            if (r.length() == 0) return false;
            if (r.equals("y"))
                dumpFile(data, graph);
            else
            {
                NEW_FILE:
                while (true)
                {
                    String f = IO.prompt("Enter the name for the new file");
                    if (f.length() == 0) return false;

                    if (Global.DEBUG && f.equals("stdout"))
                    {
                        dumpFile(System.out, graph);
                        break NEW_FILE;
                    }

                    File newFile = new File(f);
                    if (newFile.createNewFile())
                    {
                        dumpFile(newFile, graph);
                        break NEW_FILE;
                    }
                    else
                        System.out.println("That file already exists.");
                }
            }
        }
        return true;
    }

    /**
     * Prompts the user for a pair of integers representing an edge.
     * @return A Pair of the integers, or null if the user backed out.
     */
    public static Pair<Integer, Integer> promptForEdge(Graph g)
    {
        int v = Graph.NOT_VERTEX;
        int w = Graph.NOT_VERTEX;

        v = promptForLocation("Enter the first location", g);
        if (v == Graph.NOT_VERTEX) return null;

        w = promptForLocation("Enter the other location", g);
        if (w == Graph.NOT_VERTEX) return null;

        if (v == w)
        {
            System.out.println("Self-loops are not permitted");
            return null;
        }

        return new Pair<Integer, Integer>( Math.min(v, w), Math.max(v, w) );
    }

    public static void main(String[] args) throws IOException
    {
        // These variables are meant to stick around
        Graph graph;
        File data;

        try
        {
            data  = new File( IO.prompt("Enter the route data file") );
            graph = loadFile(data);
        }
        catch (Exception e)
        {
            Util.debugException(e);
            return;
        }

        IO_LOOP:
        while (true)
        {
            System.out.println("Select an action.");
            for (MenuOpt m : MenuOpt.values())
                System.out.println(m.toString());

            // Get action from user
            MenuOpt opt = getOpt();
            if (opt == null) // Invalid action selected
                continue IO_LOOP;

            SWITCH:
            switch (opt)
            {
                case SHOW: // Show the current graph
                {
                    System.out.println(
                        "The " + graph.E() + " direct flights between the "
                        + graph.V() + " locations are as follows:"
                    );
                    System.out.print(graph.toString());
                    break SWITCH;
                }

                case DMST: // Show the minimum spanning tree by distance
                {
                    PrimMST mst = new PrimMST(graph);
                    CC cc       = new CC(graph);
                    if (cc.count() > 1)
                        System.out.println(
                            "There are " + cc.count()
                            + " separate flight networks."
                        );
                    /* This is inefficient, but its worst cases tend to balance
                       one another out: more connected components means fewer
                       edges, and vice versa.
                       A trickier approach could be faster (something like
                       v*lg(v) time and v space) but it would be a nuisance to
                       implement given the existing APIs for CC and PrimMST. */
                    for (int i = 0; i < cc.count(); ++i)
                    {
                        System.out.println("The MST of component " + i + " contains:");
                        int mstSum = 0;
                        for (Edge e : mst.edges())
                        {
                            int v = e.either();
                            if (cc.id(v) == i)
                            {
                                System.out.printf(
                                    "%s to %s : %d%n",
                                    graph.nameOf(v),
                                    graph.nameOf(e.other(v)),
                                    e.getDistance()
                                );
                                mstSum += e.getDistance();
                            }
                        }
                        System.out.println("and has a total weight of " + mstSum);
                    }
                    break SWITCH;
                }

                // Shortest paths
                /* It would be trivial to make this find the shortest hops
                   path as well, but the spec says we have to use BFS. */
                case SPBM:
                case SPBP:
                {
                    Pair<Integer, Integer> edge = promptForEdge(graph);
                    if (edge == null) break SWITCH;
                    int start = edge.left;
                    int end   = edge.right;

                    /* The algorithm remains the same for both path searches.
                    What changes is how the algorithm retrieves the cost for
                    each edge. */
                    Function<Edge, Integer> getWeight =
                          opt == MenuOpt.SPBM
                        ? Edge.getDist
                        : Edge.getPrice;

                    DijkstraUndirectedSP sp = new DijkstraUndirectedSP(
                        graph,
                        start,
                        getWeight
                    );

                    Iterable<Edge> path = sp.pathTo(end);
                    if (path == null)
                    {
                        System.out.println(
                            "There is no flight path between these two locations."
                        );
                        break SWITCH;
                    }

                    if (opt == MenuOpt.SPBM)
                        System.out.println(
                            "The shortest flight has a distance of "
                            + (int)sp.distTo(end)
                            + " and is as follows:"
                        );
                    else
                        System.out.println(
                            "The cheapest flight has a cost of "
                            + Prices.toString((int)sp.distTo(end))
                            + " and is as follows:"
                        );

                    if (opt == MenuOpt.SPBM)
                        Paths.printPathByMiles(start, path, graph);
                    else
                        Paths.printPathByPrice(start, path, graph);

                    break SWITCH;
                }

                case SPBH:
                {
                    Pair<Integer, Integer> edge = promptForEdge(graph);
                    if (edge == null) break SWITCH;
                    final int start = edge.left;
                    final int end   = edge.right;
                    final String startName = graph.nameOf(start);
                    final String endName   = graph.nameOf(end);
                    BreadthFirstPaths bfs = new BreadthFirstPaths(graph, start);
                    if (!bfs.hasPathTo(end))
                    {
                        System.out.printf(
                            "There is no path from %s to %s.%n",
                            startName,
                            endName
                        );
                    }
                    else
                    {
                        final int d = bfs.distTo(end);
                        System.out.printf(
                            "The shortest trip from %s to %s has %d hop%s and"
                            + " is as follows:%n",
                            startName,
                            endName,
                            d,
                            (d == 1 ? "" : "s")
                        );
                        for (Iterator<Integer> i = bfs.pathTo(end).iterator();
                            i.hasNext(); )
                        {
                            System.out.print(graph.nameOf(i.next()));
                            if (i.hasNext()) System.out.print(" to ");
                        }
                    }
                    break SWITCH;
                }

                case CHPR: // List all cheaper routes
                {
                    Integer price = promptForPrice("Enter the maximum price");
                    if (price == null) break SWITCH;
                    TripFinder tf = new TripFinder(price, graph);
                    tf.findTrips();
                    break SWITCH;
                }

                case ADDR: // Add a route
                {
                    Integer dist;  // Distance of new edge, weight1
                    Integer price; // Price of new edge, weight2

                    Pair<Integer, Integer> edge = promptForEdge(graph);
                    if (edge == null) break SWITCH;
                    int v = edge.left;
                    int w = edge.right;

                    dist = IO.promptNum(
                        "Enter the distance in miles between the locations"
                    );
                    if (dist == null) break SWITCH;

                    price = promptForPrice("Enter the price of the flight");
                    if (price == null) break SWITCH;

                    boolean found = false;
                    EDGES:
                    for (Edge e : graph.edges())
                    {
                        if (e.either() == v && e.other(v) == w
                        ||  e.either() == w && e.other(w) == v)
                        {
                            System.out.println("Replacing existing path with new version");
                            e.setWeight(dist);
                            e.setPrice(price);
                            found = true;
                            break EDGES;
                        }
                    }
                    if (!found)
                        graph.addEdge(new Edge(v, w, dist, price));

                    break SWITCH;
                }

                case REMR: // Remove a route
                {
                    Pair<Integer, Integer> vs = promptForEdge(graph);
                    if (vs == null) break SWITCH;
                    Edge e = new Edge(vs.left, vs.right, -1, -1);
                    if (!graph.remove(e))
                        System.out.println(
                            "Could not find an edge between these locations."
                        );

                    break SWITCH;
                }

                /* Exit from the program, saving changes to the current file */
                case QUIT:
                {
                    if (quitMenu(data, graph))
                        return;
                    else
                        break SWITCH;
                }
            }
            System.out.println();
        }
    }
}
