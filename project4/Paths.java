/** Functions for manipulating paths */
package project4;
public class Paths
{
    private Paths() { }

    public static void printPathByMiles(int start, Iterable<Edge> path, Graph g)
    {
        int lastStop = start;
        System.out.print(g.nameOf(start));
        for (Edge e : path)
        {
            int v = e.other(lastStop);
            System.out.printf(" to %s (%d)", g.nameOf(v), e.getDistance());
            lastStop = v;
        }
    }

    public static void printPathByPrice(int start, Iterable<Edge> path, Graph g)
    {
        int lastStop = start;
        System.out.print(g.nameOf(start));
        for (Edge e : path)
        {
            int v = e.other(lastStop);
            System.out.printf(
                " to %s (%s)",
                g.nameOf(v),
                Prices.toString(e.getPrice())
            );
            lastStop = v;
        }
    }
}
