package project4;
import java.util.ArrayDeque;
import java.util.Deque;
public class TripFinder
{
    boolean[] marked;
    Graph g;
    final int maxPrice;

    public TripFinder(final int maxPrice, Graph g)
    {
        marked = new boolean[g.V()]; // All false
        this.g = g; // Handy to have around
        this.maxPrice = maxPrice;
    }

    /**
     * Finds all trips cheaper than the given cost by performing a
     * backtracking/pruning DFS from every location.
     */
    public void findTrips()
    {
        for (int v = 0; v < g.V(); ++v)
            dfs(v, 0, new ArrayDeque<Edge>(), v);
    }

    private void dfs(int v, int accP, Deque<Edge> path, final int start)
    {
        marked[v] = true;
        if (accP <= maxPrice && !path.isEmpty()) // Path's price is less than max
        {
            System.out.print("For " + Prices.toString(accP) + ": ");
            Paths.printPathByPrice(start, path, g);
            System.out.println();
        }
        for (Edge e : g.adj(v))
        {
            final int w = e.other(v);
            final int p = e.getPrice() + accP;
            if (!marked[w] && p <= maxPrice)
            {
                path.addLast(e);
                dfs(w, p, path, start);
                path.removeLast();
            }
        }
        marked[v] = false;
    }

    public static void main(String[] args) throws Exception
    {
        Graph test = Airline.loadFile(new java.io.File("airline_data1.txt"));
        TripFinder t = new TripFinder(400_00, test);
        t.findTrips();
    }
}
